package comp.info.test;

import java.util.ArrayList;

import org.hibernate.Query;
import org.hibernate.Session;
import org.testng.annotations.Test;

import comp.info.CompInfo;
import comp.info.entities.*;
import comp.info.hibernate.HibernateComp;
import comp.info.utils.HibernateUtils;

public class TeamTest extends AbstractTest{

	@Test(dataProvider = "addTeams", groups = {"grTeam"})
	public void addTeams2(Team pl) {
		System.out.println("start");
		System.out.println(pl.getName());
		Session session = HibernateUtils.getSessionFactory().openSession();
		String name = pl.getName();
		Query qr = session.createQuery("from Team where name = :name")
				.setString("name", name);
		
		assertTrue(qr.list().isEmpty());// there is no Team with such name

		CompInfo comp = new HibernateComp();
		comp.addTeam(pl);

		Team plDB = (Team) qr.list().get(0);

		session.flush();
		session.close();

		assertEquals(pl.getId(), plDB.getId());
	}

	@Test(dataProvider = "addTeams", groups = {"grTeam"} ,dependsOnMethods = { "addTeams2" })
	public void updateTeams(Team pl) {
		Session session = HibernateUtils.getSessionFactory().openSession();
		String name = pl.getName();
		Query qrName = session.createQuery("from Team where name = :name");
		qrName.setString("name", name);

		Team team = (Team) qrName.list().get(0);

		int id = team.getId();

		team.setName("Test TEAM Name 1 UPDATED");

		session.close();

		CompInfo comp = new HibernateComp();
		comp.updateTeam(team);

		session = HibernateUtils.getSessionFactory().openSession();
		Query qrId = session.createQuery("from Team where id = :id");
		qrId.setInteger("id", id);

		Team teamUpd = (Team) qrId.list().get(0);
		session.close();
		assertEquals(teamUpd.getName(), "Test TEAM Name 1 UPDATED");

	}

	@Test(dataProvider = "rmTeams", groups = {"grTeam"}, dependsOnMethods = { "addTeams2",
			"updateTeams" })
	public void removeTeam(String plName) {
		Session session = HibernateUtils.getSessionFactory().openSession();
		CompInfo comp = new HibernateComp();

		Query qr = session.createQuery("from Team where name = :name");
		qr.setString("name", plName);
		@SuppressWarnings("unchecked")
		ArrayList<Team> Teams = (ArrayList<Team>) qr.list();
		session.close();
		for (Team pl : Teams)
			comp.rmTeam(pl);

		session = HibernateUtils.getSessionFactory().openSession();
		qr = session.createQuery("from Team where name = :name");
		qr.setString("name", plName);
		@SuppressWarnings("unchecked")
		ArrayList<Team> clDBs = (ArrayList<Team>) qr.list();
		session.close();

		assertTrue(clDBs.isEmpty());
	}

}
