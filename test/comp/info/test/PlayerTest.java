package comp.info.test;

import java.util.ArrayList;

import org.hibernate.Query;
import org.hibernate.Session;
import org.testng.annotations.Test;

import comp.info.utils.HibernateUtils;

import comp.info.CompInfo;
import comp.info.entities.*;
import comp.info.hibernate.HibernateComp;

public class PlayerTest extends AbstractTest {

	@Test(dataProvider = "addPlayers")
	public void addPlayers2(Player pl) {
		System.out.println("start");
		System.out.println(pl.getName());
		Session session = HibernateUtils.getSessionFactory().openSession();
		String name = pl.getName();
		Query qr = session.createQuery("from Player where name = :name")
				.setString("name", name);

		assertTrue(qr.list().isEmpty());// there is no Player with such name

		CompInfo comp = new HibernateComp();
		comp.addPlayer(pl);

		Player plDB = (Player) qr.list().get(0);

		session.flush();
		session.close();

		assertEquals(pl.getId(), plDB.getId());
	}

	@Test(dataProvider = "addPlayers", dependsOnMethods = { "addPlayers2" })
	public void updatePlayers(Player pl) {
		Session session = HibernateUtils.getSessionFactory().openSession();
		String name = pl.getName();
		Query qrName = session.createQuery("from Player where name = :name");
		qrName.setString("name", name);

		Player player = (Player) qrName.list().get(0);

		int id = player.getId();

		player.setName("Test Name 1 UPDATED");

		session.close();

		CompInfo comp = new HibernateComp();
		comp.updatePlayer(player);

		session = HibernateUtils.getSessionFactory().openSession();
		Query qrId = session.createQuery("from Player where id = :id");
		qrId.setInteger("id", id);

		Player clUpd = (Player) qrId.list().get(0);
		session.close();
		assertEquals(clUpd.getName(), "Test Name 1 UPDATED");

	}

	@Test(dataProvider = "rmPlayers", dependsOnMethods = { "addPlayers2",
			"updatePlayers" })
	public void removePlayer(String plName) {
		Session session = HibernateUtils.getSessionFactory().openSession();
		CompInfo comp = new HibernateComp();

		Query qr = session.createQuery("from Player where name = :name");
		qr.setString("name", plName);
		@SuppressWarnings("unchecked")
		ArrayList<Player> Players = (ArrayList<Player>) qr.list();
		session.close();
		for (Player pl : Players)
			comp.rmPlayer(pl);

		session = HibernateUtils.getSessionFactory().openSession();
		qr = session.createQuery("from Player where name = :name");
		qr.setString("name", plName);
		@SuppressWarnings("unchecked")
		ArrayList<Player> clDBs = (ArrayList<Player>) qr.list();
		session.close();

		assertTrue(clDBs.isEmpty());
	}

	@Test(dependsOnGroups = { "grTeam" })
	public void TeamPlayers() {
		String name = "Test Name2";
		CompInfo comp = new HibernateComp();
		
		Team tm = new Team();
		tm.setName("Test Team Name 2");
		tm.setCoach_name("Test_coach__2");
		comp.addTeam(tm);
		
		Session session = HibernateUtils.getSessionFactory().openSession();
		Query qr = session.createQuery("from Player where curr_team_id = :team_id")
				.setString("team_id", Integer.toString((tm.getId())) );
		
		assertTrue(qr.list().isEmpty());
		assertTrue(comp.loadPlayers(tm.getId()).isEmpty());
		for (int i = 0; i < 5; i++) {
			Player pl1 = new Player();
			pl1.setName(name);
			pl1.setAge(20);
			pl1.setCurr_team_id(tm.getId());
			comp.addPlayer(pl1);
		}
		@SuppressWarnings("unchecked")
		ArrayList<Player> players = (ArrayList<Player>) qr.list();
		ArrayList<Player> players2 = comp.loadPlayers(tm.getId());
		assertEquals(qr.list().size(),comp.loadPlayers(tm.getId()).size());
		
		for (int i=0; i<players.size(); i++)
		{
			assertEquals(players.get(i).getId(), players2.get(i).getId());
			comp.rmPlayer(players.get(i));
		}
		
		session.flush();
		session.close();
		comp.rmTeam(tm);
	}

}
