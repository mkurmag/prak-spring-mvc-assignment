package comp.info.test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.DataProvider;

import comp.info.entities.*;

public abstract class AbstractTest extends Assert {
	@DataProvider
	protected static Iterator<Object[]> addPlayers() {
		Player pl1 = new Player();
		pl1.setName("Test Name1");
		pl1.setAge(20);

		List<Object[]> res = new ArrayList<Object[]>();
		res.add(new Object[] { pl1 });
		return res.iterator();
	}

	@DataProvider
	protected static Iterator<Object[]> rmPlayers() {
		List<Object[]> res = new ArrayList<Object[]>();

		Iterator<Object[]> addedPlayers = addPlayers();
		while (addedPlayers.hasNext()) {
			Object[] elem = addedPlayers.next();
			String plName = ((Player) elem[0]).getName();
			res.add(new Object[] { plName });
		}
		res.add(new Object[] { "Test Name 1 UPDATED" });
		return res.iterator();
	}

	@DataProvider
	protected static Iterator<Object[]> addTeams() {
		Team pl1 = new Team();
		pl1.setName("Test Team Name1");
		pl1.setCoach_name("Test_coach");

		List<Object[]> res = new ArrayList<Object[]>();
		res.add(new Object[] { pl1 });
		return res.iterator();
	}

	@DataProvider
	protected static Iterator<Object[]> rmTeams() {
		List<Object[]> res = new ArrayList<Object[]>();

		Iterator<Object[]> addedTeams = addTeams();
		while (addedTeams.hasNext()) {
			Object[] elem = addedTeams.next();
			String plName = ((Team) elem[0]).getName();
			res.add(new Object[] { plName });
		}
		res.add(new Object[] { "Test Team Name 1 UPDATED" });
		return res.iterator();
	}

}
