package comp.info.test;

import org.hibernate.Session;
import org.testng.Assert;
import org.testng.annotations.Test;

import comp.info.utils.HibernateUtils;

public class CommonTest extends Assert {
	@Test
	public void connectivityTest() {
		Session session = HibernateUtils.getSessionFactory().openSession();
		assertTrue(session.isConnected());
	}

}
