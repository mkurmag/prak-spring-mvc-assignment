<%@ include file="/WEB-INF/jsp/header.jsp" %>
<%@ include file="/WEB-INF/jsp/includes.jsp" %>

<h2>Team "${team.name}" </h2>

<h3> ${team.coach_name} is the coach </h3>

<c:choose>
	<c:when test="${fn:length(players)>0}">
		<h4>Players:</h4>
		<table>
			<thead>
				<tr>
					<th>Name</th>
					<th>Age</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="player" items="${players}">
					<tr>
						<td><a href="player.do?playerId=${player.id}">${player.name}</a></td>
						<td>${player.age}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</c:when>
	<c:otherwise>
		<h4>This team has not players yet</h4>
	</c:otherwise>
</c:choose>

<br></br>
	
<c:if test="${isloggedin == true}">
	<a href="updateTeam.do?teamId=${team.id}">
		<button>Edit team</button>
	</a>
</c:if>
<%@ include file="/WEB-INF/jsp/footer.jsp"%>