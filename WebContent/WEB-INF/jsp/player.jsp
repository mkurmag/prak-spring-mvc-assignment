<%@ include file="/WEB-INF/jsp/includes.jsp" %>
<%@ include file="/WEB-INF/jsp/header.jsp" %>

<h2>Player</h2>

<table>
	<tr>
		<th>Name</th>
		<td>${player.name}</td>
	</tr>
	<tr>
		<th>Age</th>
		<td>${player.age}</td>
	</tr>
	<tr>
		<th>Team</th>
		<td>${command}</td>
	</tr>
</table>

<br></br>
<c:if test="${isloggedin == true}">
	<a href="updatePlayer.do?playerId=${player.id}">
		<button>Edit player</button>
	</a>
</c:if>
<%@ include file="/WEB-INF/jsp/footer.jsp" %>