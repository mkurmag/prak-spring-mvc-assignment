<%@ include file="/WEB-INF/jsp/includes.jsp" %>
<%@ include file="/WEB-INF/jsp/header.jsp" %>

<h2>
	<c:choose>
		<c:when test="${team.isNew()}"> Add team </c:when>
		<c:otherwise>Edit team </c:otherwise>
	</c:choose>
</h2>

<form:form modelAttribute="team">
	<table>
		<tr>
			<th>Name</th>
			<td><form:input path="name" /></td>
		</tr>
		<tr>
			<th>Coach</th>
			<td><form:input path="coach_name" /></td>
		</tr>
	</table>

	<tr>
		<td>
			<p class="submit">
				<c:choose>
				<c:when test="${team.isNew()}"> <input type="submit" value="Add team" /></c:when>
			    <c:otherwise><input type="submit" value="Save edit" /></c:otherwise>
			    </c:choose>
			</p>
		</td>
	</tr>
</form:form>

<c:if test="${!team.isNew()}">
	<a href="removeTeam.do?teamId=${team.id}">
		<button>Remove this team</button>
	</a>
</c:if>

<%@ include file="/WEB-INF/jsp/footer.jsp" %>
