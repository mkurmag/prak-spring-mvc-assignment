<%@ include file="/WEB-INF/jsp/includes.jsp" %>
<%@ include file="/WEB-INF/jsp/header.jsp" %>

<h3>Login</h3>

	<c:if test="${error}">
		<div class="errorblock">
			Your login/password is incorrect. Try again.
		</div>
	</c:if>

<form:form method="POST" commandName="login" modelAttribute="login">
	<table>
		<tr>
			<td>Login:</td>
			<td><form:input path="login" /></td>
		</tr>
		<tr>
			<td>Password:</td>
			<td><form:password path="pass" /></td>
		</tr>
		<tr>
			<td colspan='2'>
				<p class="submit">
					<input type="submit" value="login" />
				</p>
			</td>
		</tr>
	</table>
</form:form>

<%@ include file="/WEB-INF/jsp/footer.jsp" %>