<%@ include file="/WEB-INF/jsp/includes.jsp" %>
<%@ include file="/WEB-INF/jsp/header.jsp" %>

<h2>
	<c:choose>
		<c:when test="${player.isNew()}"> Add player </c:when>
		<c:otherwise>Edit player </c:otherwise>
	</c:choose>
</h2>

<form:form modelAttribute="player">
	<table>
		<tr>
			<th>Name</th>
			<td><form:input path="name" /></td>
		</tr>
		<tr>
			<th>Age</th>
			<td><form:input path="age" /></td>
		</tr>
		<tr>
      <th>Team</th>
      <td>
		<form:select path="curr_team_id">
			<form:options items="${teams}" />
		</form:select>
		</td>
    </tr>
	</table>

	<tr>
		<td>
			<p class="submit">
				<c:choose>
				<c:when test="${player.isNew()}"> <input type="submit" value="Add player" /></c:when>
			    <c:otherwise><input type="submit" value="Save edit" /></c:otherwise>
			    </c:choose>
			</p>
		</td>
	</tr>
</form:form>

<c:if test="${!player.isNew()}">
	<a href="removePlayer.do?playerId=${player.id}">
		<button>Remove this player</button>
	</a>
</c:if>

<%@ include file="/WEB-INF/jsp/footer.jsp" %>
