<%@ include file="/WEB-INF/jsp/header.jsp" %>
<%@ include file="/WEB-INF/jsp/includes.jsp" %>

<h2>All players</h2>

<table>
	<thead>
		<tr>
			<th>Name</th>
			<th>Age</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="player" items="${players}">
			<tr>
				<td><a href="player.do?playerId=${player.id}">${player.name}</a></td>
				<td>${player.age}</td>
			</tr>
		</c:forEach>
	</tbody>
</table>


<%@ include file="/WEB-INF/jsp/footer.jsp"%>