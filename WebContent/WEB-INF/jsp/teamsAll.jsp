<%@ include file="/WEB-INF/jsp/header.jsp" %>
<%@ include file="/WEB-INF/jsp/includes.jsp" %>

<h2>All teams</h2>

<table>
	<thead>
		<tr>
			<th>Name</th>
			<th>Coach</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="team" items="${teams}">
			<tr>
				<td><a href="team.do?teamId=${team.id}">${team.name}</a></td>
				<td>${team.coach_name}</td>
			</tr>
		</c:forEach>
	</tbody>
</table>

<%@ include file="/WEB-INF/jsp/footer.jsp"%>