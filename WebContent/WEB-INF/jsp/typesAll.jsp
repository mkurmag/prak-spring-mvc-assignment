<%@ include file="/WEB-INF/jsp/header.jsp" %>
<%@ include file="/WEB-INF/jsp/includes.jsp" %>

<h2>All sports</h2>

<table>
	<thead>
		<tr>
			<th>Sport name</th>
			<th>is team sport?</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="type" items="${types}">
			<tr>
				<td><a href="type.do?typeId=${type.id}">${type.name}</a></td>
				<td>
				<c:choose>
				<c:when test="${type.isTeam}">yes</c:when>
				<c:otherwise>no</c:otherwise>
				</c:choose>
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>


<%@ include file="/WEB-INF/jsp/footer.jsp"%>