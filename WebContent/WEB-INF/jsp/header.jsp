<%@ include file="/WEB-INF/jsp/includes.jsp" %>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <link rel="stylesheet" href="<c:url value="/styles/comp.css"/>" type="text/css"/>
  <title>Competition Information</title>
</head>


<body>
	<h1>Competition information site</h1>
	

	<div id="login__">
		<c:choose>
			<c:when test="${isloggedin == true}">
			You was logged in as administrator
			<br>
				<a href="<c:url value="/logout.do"/>">Logout</a>
			</c:when>
			<c:otherwise>
				<a href="<c:url value="/login.do"/>">Login</a>
			</c:otherwise>
		</c:choose>
	</div>

	<div id="main">
		<div class="menu">
			<ul>
				<li><a href="<c:url value="/allPlayer.do"/>">Show all player</a></li>
				<li><a href="<c:url value="/allTeam.do"/>">Show all teams</a></li>
				<li><a href="<c:url value="/allType.do"/>">Show all sports</a></li>
			</ul>
			<ul>
				<li><a href="<c:url value="/addTeam.do"/>">Add team</a></li>
				<li><a href="<c:url value="/addPlayer.do"/>">Add player</a></li>
				<li><a href="<c:url value="/addType.do"/>">Add sport</a></li>
			</ul>	
		</div>
	

		<div class="base">