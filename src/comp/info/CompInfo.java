package comp.info;

import java.util.ArrayList;

import comp.info.entities.Player;
import comp.info.entities.Team;
import comp.info.entities.Type;

public interface CompInfo {
	void addPlayer(Player pl);
	void rmPlayer(Player pl);
	void updatePlayer(Player pl);
	
	Player loadPlayer(Integer player_id);
	ArrayList<Player> loadPlayers(Integer team_id);
	ArrayList<Player> getAllPlayers();
	
	void addTeam(Team t);
	void rmTeam(Team t);
	void updateTeam(Team t);
	Team loadTeam(Integer team_id);
	ArrayList<Team> getAllTeams();
	
	void addType(Type t);
	void rmType(Type t);
	void updateType(Type t);
	Type loadType(Integer team_id);
	ArrayList<Type> getAllTypes();
	
	boolean isLogged();
	void setLogin(boolean isLogin);
}
