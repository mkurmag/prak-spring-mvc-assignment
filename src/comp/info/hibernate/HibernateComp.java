package comp.info.hibernate;

import java.util.ArrayList;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Service;

import comp.info.CompInfo;
import comp.info.entities.Player;
import comp.info.entities.Team;
import comp.info.entities.Type;
import comp.info.utils.HibernateUtils;

//TODO there is duplicated finally-code and Transaction, Session
@Service
@ManagedResource("CompInfo:type=CompInfo")
public class HibernateComp implements CompInfo {
	private boolean logged = false;

	public void addPlayer(Player pl) {
		Transaction tr = null;
		Session session = HibernateUtils.getSessionFactory().openSession();
		try {
			tr = session.beginTransaction();
			session.save(pl);
			tr.commit();
		} finally {
			if (tr != null && !tr.wasCommitted())
				tr.rollback();
			session.flush();
			session.close();
		}
	}

	public void rmPlayer(Player pl) {
		// TODO removing orders of Player
		Transaction tr = null;
		Session session = HibernateUtils.getSessionFactory().openSession();
		try {
			tr = session.beginTransaction();
			session.delete(pl);
			System.out.println("Delete Player with id = " + pl.getId());
			tr.commit();
		} finally {
			if (tr != null && !tr.wasCommitted())
				tr.rollback();
			session.flush();
			session.close();
		}
	}

	public void updatePlayer(Player pl) {
		Transaction tr = null;
		Session session = HibernateUtils.getSessionFactory().openSession();
		try {
			tr = session.beginTransaction();
			session.update(pl);
			tr.commit();
		} finally {
			if (tr != null && !tr.wasCommitted())
				tr.rollback();
			session.flush();
			session.close();
		}
	}

	public Player loadPlayer(Integer id) {
		Session session = HibernateUtils.getSessionFactory().openSession();
		try {
			@SuppressWarnings("unchecked")
			ArrayList<Player> prs = (ArrayList<Player>) session
					.createQuery("from Player where id=:ID")
					.setInteger("ID", id).list();
			if (prs.isEmpty())
				return null;
			return prs.get(0);
		} finally {
			session.close();
		}
	}

	public boolean isLogged() {
		return logged;
	}

	public void addTeam(Team pl) {
		Transaction tr = null;
		Session session = HibernateUtils.getSessionFactory().openSession();
		try {
			tr = session.beginTransaction();
			session.save(pl);
			tr.commit();
		} finally {
			if (tr != null && !tr.wasCommitted())
				tr.rollback();
			session.flush();
			session.close();
		}
	}

	public void rmTeam(Team pl) {
		Transaction tr = null;
		Session session = HibernateUtils.getSessionFactory().openSession();
		try {
			tr = session.beginTransaction();
			session.delete(pl);
			tr.commit();
		} 
		catch(HibernateException ex){
			System.out.println("Errroro" + ex);
			System.out.println("Errroro" + ex);
		}
		finally {
			
			if (tr != null && !tr.wasCommitted())
				tr.rollback();
			session.flush();
			session.close();
		}
	}

	public void updateTeam(Team pl) {
		Transaction tr = null;
		Session session = HibernateUtils.getSessionFactory().openSession();
		try {
			tr = session.beginTransaction();
			session.update(pl);
			tr.commit();
		} finally {
			if (tr != null && !tr.wasCommitted())
				tr.rollback();
			session.flush();
			session.close();
		}
	}

	
	public ArrayList<Player> loadPlayers(Integer team_id) {
		Session session = HibernateUtils.getSessionFactory().openSession();
		try {
			@SuppressWarnings("unchecked")
			ArrayList<Player> prs = (ArrayList<Player>) session
					.createQuery("from Player where curr_team_id=:tID")
					.setInteger("tID", team_id).list();
			return prs;
		} finally {
			session.close();
		}
	}

	
	public Team loadTeam(Integer team_id) {
		Session session = HibernateUtils.getSessionFactory().openSession();
		try {
			@SuppressWarnings("unchecked")
			ArrayList<Team> prs = (ArrayList<Team>) session
					.createQuery("from Team where id=:ID")
					.setInteger("ID", team_id).list();
			if (prs.isEmpty())
				return null;
			return prs.get(0);
		} finally {
			session.close();
		}
	}

	
	public void setLogin(boolean isLogin) {
		this.logged=isLogin;
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<Team> getAllTeams() {
		ArrayList<Team> prs = null;
		Session session = HibernateUtils.getSessionFactory().openSession();
		try {
			prs = (ArrayList<Team>) session.createQuery("from Team").list();
		} finally {
			session.flush();
			session.close();
		}
		return prs;
	}	
	
	@SuppressWarnings("unchecked")
	public ArrayList<Player> getAllPlayers() {
		ArrayList<Player> prs = null;
		Session session = HibernateUtils.getSessionFactory().openSession();
		try {
			prs = (ArrayList<Player>) session.createQuery("from Player").list();
		} finally {
			session.flush();
			session.close();
		}
		return prs;
	}

/*
 * Type operations
 */	
	
	public void addType(Type pl) {
		Transaction tr = null;
		Session session = HibernateUtils.getSessionFactory().openSession();
		try {
			tr = session.beginTransaction();
			session.save(pl);
			tr.commit();
		} finally {
			if (tr != null && !tr.wasCommitted())
				tr.rollback();
			session.flush();
			session.close();
		}
	}

	public void rmType(Type pl) {
		Transaction tr = null;
		Session session = HibernateUtils.getSessionFactory().openSession();
		try {
			tr = session.beginTransaction();
			session.delete(pl);
			System.out.println("Delete Type with id = " + pl.getId());
			tr.commit();
		} finally {
			if (tr != null && !tr.wasCommitted())
				tr.rollback();
			session.flush();
			session.close();
		}
	}

	public void updateType(Type pl) {
		Transaction tr = null;
		Session session = HibernateUtils.getSessionFactory().openSession();
		try {
			tr = session.beginTransaction();
			session.update(pl);
			tr.commit();
		} finally {
			if (tr != null && !tr.wasCommitted())
				tr.rollback();
			session.flush();
			session.close();
		}
	}

	public Type loadType(Integer id) {
		Session session = HibernateUtils.getSessionFactory().openSession();
		try {
			@SuppressWarnings("unchecked")
			ArrayList<Type> prs = (ArrayList<Type>) session
					.createQuery("from Type where id=:ID")
					.setInteger("ID", id).list();
			if (prs.isEmpty())
				return null;
			return prs.get(0);
		} finally {
			session.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<Type> getAllTypes() {
		ArrayList<Type> prs = null;
		Session session = HibernateUtils.getSessionFactory().openSession();
		try {
			prs = (ArrayList<Type>) session.createQuery("from Type").list();
		} finally {
			session.flush();
			session.close();
		}
		return prs;
	}
	
	/*
	 * 
	 */
	
}
