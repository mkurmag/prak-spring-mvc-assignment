package comp.info.entities;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class Type {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Integer id;
	String name;
	Boolean isTeam;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getIsTeam() {
		return isTeam;
	}

	public void setIsTeam(Boolean isTeam) {
		System.out.println("BOOLEAN");
		this.isTeam = isTeam;
	}
}
