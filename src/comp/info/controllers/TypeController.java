package comp.info.controllers;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import comp.info.CompInfo;
import comp.info.entities.Type;
import comp.info.entities.Team;

@Controller
public class TypeController {
	private final CompInfo comp;

	@Autowired
	public TypeController(CompInfo comp) {
		this.comp = comp;
		
	}

	@RequestMapping("/type.do")
	public String setupForm2(@RequestParam("typeId") int id, Model model) {
		Type pl = this.comp.loadType(id);
		model.addAttribute("type", pl);
		
		model.addAttribute("isloggedin", comp.isLogged());
		return "type";
	}
	
	@RequestMapping("/allType.do")
	public String setupForm(Model model) {
		
		ArrayList<Type> pls = comp.getAllTypes();
		model.addAttribute("types", pls);
		model.addAttribute("isloggedin", comp.isLogged());
		
		return "typesAll";
	}

	@RequestMapping(value="/updateType.do", method = RequestMethod.GET)
	public String setupFrom(@RequestParam("typeId") int id, Model model) 
	{
		Type pl = this.comp.loadType(id);
		model.addAttribute("type", pl);
		
		LinkedHashMap<Integer, String> tmp_teams = new LinkedHashMap<Integer, String>();
		for (Team pr: comp.getAllTeams()) {
			tmp_teams.put(pr.getId(), pr.getName());
		}
		model.addAttribute("teams", tmp_teams);
		
		model.addAttribute("isloggedin", comp.isLogged());
		return "typeForm";
	}

	@RequestMapping(value = "/updateType.do", method = RequestMethod.POST)
	public String process(@RequestParam("typeId") int id, Type type, Model model) {

		type.setId(id);
		comp.updateType(type);
		return "redirect:type.do?typeId=" + id;
	}

	@RequestMapping(value = "/removeType.do")
	// , method = RequestMethod.GET)
	public String processRemove(@RequestParam("typeId") int id) {
		this.comp.rmType(this.comp.loadType(id));
		return "redirect:welcome.do";
	}
	
	@RequestMapping(value = "/addType.do", method = RequestMethod.GET)
	public String setupAddProductForm(Model model) {
		Type type = new Type();
		model.addAttribute("type", type);

		LinkedHashMap<Integer, String> tmp_teams = new LinkedHashMap<Integer, String>();
		for (Team pr: comp.getAllTeams()) {
			tmp_teams.put(pr.getId(), pr.getName());
		}
		model.addAttribute("teams", tmp_teams);
		
		model.addAttribute("isloggedin", comp.isLogged());
		return "typeForm";
	}

	@RequestMapping(value = "/addType.do", method = RequestMethod.POST)
	public String processAddProductForm(@ModelAttribute Type type) {
		this.comp.addType(type);
		return "redirect:type.do?typeId=" + type.getId();
	}

}
