package comp.info.controllers;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import comp.info.CompInfo;
import comp.info.entities.Player;
import comp.info.entities.Team;

@Controller
public class PlayerController {
	private final CompInfo comp;

	@Autowired
	public PlayerController(CompInfo comp) {
		this.comp = comp;
		
	}

	@RequestMapping("/player.do")
	public String setupForm2(@RequestParam("playerId") int id, Model model) {
		Player pl = this.comp.loadPlayer(id);
		model.addAttribute("player", pl);
		
		if (pl.getCurr_team_id()!=null){
			model.addAttribute("command", comp.loadTeam(pl.getCurr_team_id()).getName());
		}else 
			model.addAttribute("command", "not comand");
		
		model.addAttribute("isloggedin", comp.isLogged());
		return "player";
	}
	
	@RequestMapping("/allPlayer.do")
	public String setupForm(Model model) {
		
		ArrayList<Player> pls = comp.getAllPlayers();
		model.addAttribute("players", pls);
		model.addAttribute("isloggedin", comp.isLogged());
		
		return "playersAll";
	}

	@RequestMapping(value="/updatePlayer.do", method = RequestMethod.GET)
	public String setupFrom(@RequestParam("playerId") int id, Model model) 
	{
		Player pl = this.comp.loadPlayer(id);
		model.addAttribute("player", pl);
		
		LinkedHashMap<Integer, String> tmp_teams = new LinkedHashMap<Integer, String>();
		for (Team pr: comp.getAllTeams()) {
			tmp_teams.put(pr.getId(), pr.getName());
		}
		model.addAttribute("teams", tmp_teams);
		
		model.addAttribute("isloggedin", comp.isLogged());
		return "playerForm";
	}

	@RequestMapping(value = "/updatePlayer.do", method = RequestMethod.POST)
	public String process(@RequestParam("playerId") int id, Player player, Model model) {

		player.setId(id);
		comp.updatePlayer(player);
		return "redirect:player.do?playerId=" + id;
	}

	@RequestMapping(value = "/removePlayer.do")
	// , method = RequestMethod.GET)
	public String processRemove(@RequestParam("playerId") int id) {
		this.comp.rmPlayer(this.comp.loadPlayer(id));
		return "redirect:welcome.do";
	}
	
	@RequestMapping(value = "/addPlayer.do", method = RequestMethod.GET)
	public String setupAddProductForm(Model model) {
		Player player = new Player();
		model.addAttribute("player", player);

		LinkedHashMap<Integer, String> tmp_teams = new LinkedHashMap<Integer, String>();
		for (Team pr: comp.getAllTeams()) {
			tmp_teams.put(pr.getId(), pr.getName());
		}
		model.addAttribute("teams", tmp_teams);
		
		model.addAttribute("isloggedin", comp.isLogged());
		return "playerForm";
	}

	@RequestMapping(value = "/addPlayer.do", method = RequestMethod.POST)
	public String processAddProductForm(@ModelAttribute Player player) {
		this.comp.addPlayer(player);
		return "redirect:player.do?playerId=" + player.getId();
	}

}
