package comp.info.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import comp.info.CompInfo;
import comp.info.additional.Login;

@Controller
public class LoginController {
	private final CompInfo comp;
	
	@Autowired
	public LoginController(CompInfo comp) {
		this.comp = comp;
	}

	@RequestMapping(value = "/login.do", method = RequestMethod.GET)
	public String login(Model model) {
		model.addAttribute("login", new Login());
		return "login";
	}

	@RequestMapping(value="/login.do", method = RequestMethod.POST)
	public String process(Login login, Model model) {
		
		if (login.getLogin().equals("admin") && login.getPass().equals("admin")){
			System.out.println("**sdf***sfd**sdf***df****");
			comp.setLogin(true);
			return "redirect:welcome.do";
		}
		else {
			model.addAttribute("error", "true");
			return "login";
		}
	}

	@RequestMapping(value = "/logout.do")
	public String logout(Model model) {
		comp.setLogin(false);
		return "redirect:welcome.do";
	}


}
