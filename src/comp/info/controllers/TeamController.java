package comp.info.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import comp.info.CompInfo;
import comp.info.entities.Player;
import comp.info.entities.Team;

@Controller
public class TeamController {
	private final CompInfo comp;

	@Autowired
	public TeamController(CompInfo comp) {
		this.comp = comp;
	}

	@RequestMapping("/team.do")
	public String setupForm2(@RequestParam("teamId") int team_id, Model model) {
		
		ArrayList<Player> pls = comp.loadPlayers(team_id);
		model.addAttribute("players", pls);
		model.addAttribute("team", comp.loadTeam(team_id));
		model.addAttribute("isloggedin", comp.isLogged());
		
		return "team";
	}
	
	@RequestMapping("/allTeam.do")
	public String setupForm(Model model) {
		
		ArrayList<Team> pls = comp.getAllTeams();
		model.addAttribute("teams", pls);
		model.addAttribute("isloggedin", comp.isLogged());
		
		return "teamsAll";
	}
	
	@RequestMapping(value="/updateTeam.do", method = RequestMethod.GET)
	public String setupFrom(@RequestParam("teamId") int id, Model model) 
	{
		model.addAttribute("team", this.comp.loadTeam(id));
		model.addAttribute("isloggedin", comp.isLogged());
		
		return "teamForm";
	}

	@RequestMapping(value="/updateTeam.do", method = RequestMethod.POST)
	public String process(@RequestParam("teamId") int id, Team team, Model model) {

		team.setId(id);
		comp.updateTeam(team);
		return "redirect:team.do?teamId=" + id;
	}
	
	@RequestMapping(value = "/removeTeam.do")//, method = RequestMethod.GET)
	public String processRemove(@RequestParam("teamId") int id) {
		this.comp.rmTeam(this.comp.loadTeam(id));
		return "redirect:welcome.do";
	}
	
	@RequestMapping(value = "/addTeam.do", method = RequestMethod.GET)
	public String setupAddProductForm(Model model) {
		Team team = new Team();
		model.addAttribute(team);
		model.addAttribute("isloggedin", comp.isLogged());
		return "teamForm";
	}

	@RequestMapping(value = "/addTeam.do", method = RequestMethod.POST)
	public String processAddProductForm(@ModelAttribute Team team) {
		this.comp.addTeam(team);
		return "redirect:team.do?teamId=" + team.getId();
	}
}
