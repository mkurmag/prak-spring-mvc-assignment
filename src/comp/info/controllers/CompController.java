package comp.info.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import comp.info.CompInfo;

@Controller
public class CompController {
	private final CompInfo comp;

	@Autowired
	public CompController(CompInfo comp) {
		this.comp = comp;
	}

	@RequestMapping("/welcome.do")
	public void welcomeHandler(Model model) {
		model.addAttribute("isloggedin", comp.isLogged());
	}

}
